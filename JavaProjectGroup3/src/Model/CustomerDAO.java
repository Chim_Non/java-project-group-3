package Model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Control.DBUtils;

public class CustomerDAO {
	private Connection connection;

	public CustomerDAO() {
		connection = DBUtils.getConnection();
	}

	public List<Customer> getAllCustomer() {
		List<Customer> customers = new ArrayList<Customer>();
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("call viewAllCustomer()");
			while (rs.next()) {
				Customer customer = new Customer();
				customer.setId(rs.getInt(1));
				customer.setName(rs.getString(2));
				customer.setPhone(rs.getString(3));
				customer.setAddress(rs.getString(4));
				customers.add(customer);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customers;
	}

	public void creatCustomer(Customer customer) {
		try {
			PreparedStatement statement = connection.prepareStatement("call creatNewCustomer(?,?,?)");
			statement.setString(1, customer.getName());
			statement.setString(2, customer.getPhone());
			statement.setString(3, customer.getAddress());
			statement.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void deleteCustomer(int id) {
		try {
			PreparedStatement statement = connection.prepareStatement("call deleteCustomer(?)");
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void updateCustomer(int id, String name, String phone, String address) throws IOException {
		try {
			PreparedStatement statement = connection.prepareStatement("call updateCustomer(?,?,?,?)");
			statement.setInt(1, id);
			statement.setString(2, name);
			statement.setString(3, phone);
			statement.setString(4, address);
			statement.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public Customer getCustomerById(int customerID) {
		Customer customer = new Customer();
		try {
			PreparedStatement statement = connection.prepareStatement("call getCustomerById(?)");
			statement.setInt(1, customerID);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				customer.setId(rs.getInt(1));
				customer.setName(rs.getString(2));
				customer.setPhone(rs.getString(3));
				customer.setAddress(rs.getString(4));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customer;
	}
	
	public int getMaxId(){
		Customer customer = new Customer();
		int maxId = 0;
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("call getMaxValueIdCustomer");
			while(rs.next()){
				maxId = (int)rs.getInt(1);
			}
			maxId++;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return maxId;
	}
	
}
