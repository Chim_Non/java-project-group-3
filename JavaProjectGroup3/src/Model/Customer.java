package Model;

public class Customer implements Person {

	private int id;
	private String name;
	private String phone;
	private String address;

	@Override
	public void setId(int id) {
		this.id = id;

	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String getPhone() {
		return phone;
	}

	@Override
	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String getAddress() {
		return address;
	}

	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}

}
