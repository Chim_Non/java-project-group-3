package Model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Control.DBUtils;

public class StaffDAO {
	private Connection connection;

	public StaffDAO() {
		connection = DBUtils.getConnection();
	}

	public List<Staff> getAllStaff() {
		List<Staff> staffs = new ArrayList<Staff>();
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("call viewAllStaff()");
			while (rs.next()) {
				Staff staff = new Staff();
				staff.setId(rs.getInt("id_staff"));
				staff.setName(rs.getString("name"));
				staff.setPhone(rs.getString("phone"));
				staff.setAddress(rs.getString("address"));
				staff.setPass(rs.getString("pass"));
				staff.setRole(rs.getInt("role"));
				staffs.add(staff);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return staffs;
	}

	public void creatCustomer(Staff staff) {
		try {
			PreparedStatement statement = connection.prepareStatement("call creatNewStaff(?,?,?,,?)");
			statement.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void deleteStaff(int id) {
		try {
			PreparedStatement statement = connection.prepareStatement("call deleteStaff(?)");
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void updateStaff(int id, String name, String phone, String address, String pass, int role) {
		try {
			PreparedStatement statement = connection.prepareStatement("call updateStaff(?,?,?,?,?,?)");
			statement.setInt(1, id);
			statement.setString(2, name);
			statement.setString(3, phone);
			statement.setString(4, address);
			statement.setString(5, pass);
			statement.setInt(6, role);
			statement.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public Staff getStaffById(int id) {
		Staff staff = new Staff();
		try {
//			PreparedStatement statement = connection.prepareStatement("call viewStaff(?)");
//			statement.executeQuery();
//			statement.setInt(1,id);
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * FROM staff where id_staff = "+id+"");
			while (rs.next()) {
				//Staff staff = new Staff();
				staff.setId(rs.getInt("id_staff"));
				staff.setName(rs.getString("name"));
				staff.setPhone(rs.getString("phone"));
				staff.setAddress(rs.getString("address"));
				staff.setPass(rs.getString("pass"));
				staff.setRole(rs.getInt("role"));
				//staffs.add(staff);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return staff;
	}
	public List<Staff> getStaffByName(String name) {
		List<Staff> staffs = new ArrayList<Staff>();
		try {
//			PreparedStatement statement = connection.prepareStatement("call viewStaff(?)");
//			statement.executeQuery();
//			statement.setInt(1,id);
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * FROM staff where name = '"+name+"'");
			while (rs.next()) {
				Staff staff = new Staff();
				staff.setId(rs.getInt("id_staff"));
				staff.setName(rs.getString("name"));
				staff.setPhone(rs.getString("phone"));
				staff.setAddress(rs.getString("address"));
				staff.setPass(rs.getString("pass"));
				staff.setRole(rs.getInt("role"));
				staffs.add(staff);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return staffs;
	}
	public List<Staff> getStaffByAddress(String address) {
		List<Staff> staffs = new ArrayList<Staff>();
		try {
//			PreparedStatement statement = connection.prepareStatement("call viewStaff(?)");
//			statement.executeQuery();
//			statement.setInt(1,id);
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * FROM staff where address = '"+address+"'");
			while (rs.next()) {
				Staff staff = new Staff();
				staff.setId(rs.getInt("id_staff"));
				staff.setName(rs.getString("name"));
				staff.setPhone(rs.getString("phone"));
				staff.setAddress(rs.getString("address"));
				staff.setPass(rs.getString("pass"));
				staff.setRole(rs.getInt("role"));
				staffs.add(staff);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return staffs;
	}
	public List<Staff> getStaffByPhone(String phone) {
		List<Staff> staffs = new ArrayList<Staff>();
		try {
//			PreparedStatement statement = connection.prepareStatement("call viewStaff(?)");
//			statement.executeQuery();
//			statement.setInt(1,id);
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * FROM staff where phone = '"+phone+"'");
			while (rs.next()) {
				Staff staff = new Staff();
				staff.setId(rs.getInt("id_staff"));
				staff.setName(rs.getString("name"));
				staff.setPhone(rs.getString("phone"));
				staff.setAddress(rs.getString("address"));
				staff.setPass(rs.getString("pass"));
				staff.setRole(rs.getInt("role"));
				staffs.add(staff);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return staffs;
	}
//	public int getIdUpdate() {
//		Staff staff = new Staff();
//		List<Staff> staffs = new ArrayList<Staff>();
//		int updateID = 0;
//		staff.setId(updateID);
//		staffs.add(staff);
//		return updateID;
//	}
	public List<Staff> getDataNext(int i){
		List<Staff> staffs = new ArrayList<Staff>();
		Statement statement;
		try {
			statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * FROM staff LIMIT "+i+", 10");
			while (rs.next()) {
				Staff staff = new Staff();
				staff.setId(rs.getInt("id_staff"));
				staff.setName(rs.getString("name"));
				staff.setPhone(rs.getString("phone"));
				staff.setAddress(rs.getString("address"));
				staff.setPass(rs.getString("pass"));
				staff.setRole(rs.getInt("role"));
				staffs.add(staff);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return staffs;
	}
	public int getMaxId() {
		Staff staff = new Staff();
		int maxID = 0;
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT MAX(id_staff) FROM staff");
			while (rs.next()) {
				maxID = rs.getInt("MAX(id_staff)");
			}
			maxID = maxID + 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return maxID;
	}

	public void insertStaff(String name, String phone, String address, String pass, int role) {
		Staff staff = new Staff();
		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate("insert into staff(name, phone, address, pass, role) values ('" + name + "','"
					+ phone + "','" + address + "','" + pass + "'," + role + ")");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
}
