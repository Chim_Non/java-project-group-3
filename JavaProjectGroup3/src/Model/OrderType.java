package Model;

public class OrderType {
	int orderTypeId;
	String orderTypeName;
	float orderTypeRatio;

	public int getOrderTypeId() {
		return orderTypeId;
	}

	public void setOrderTypeId(int orderTypeId) {
		this.orderTypeId = orderTypeId;
	}

	public String getOrderTypeName() {
		return orderTypeName;
	}

	public void setOrderTypeName(String orderTypeName) {
		this.orderTypeName = orderTypeName;
	}

	public float getOrderTypeRatio() {
		return orderTypeRatio;
	}

	public void setOrderTypeRatio(float orderTypeRatio) {
		this.orderTypeRatio = orderTypeRatio;
	}

	public OrderType(int orderTypeId, String orderTypeName, float orderTypeRatio) {
		super();
		this.orderTypeId = orderTypeId;
		this.orderTypeName = orderTypeName;
		this.orderTypeRatio = orderTypeRatio;
	}

	public OrderType() {
		super();
		// TODO Auto-generated constructor stub
	}

}
