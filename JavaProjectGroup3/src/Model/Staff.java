package Model;

public class Staff implements Person {
	private int id;
	private String name;
	private String phone;
	private String address;
	private String pass;
	private int role;

	@Override
	public void setId(int id) {
		this.id = id;

	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String getPhone() {
		return phone;
	}

	@Override
	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String getAddress() {
		return address;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public Staff() {
		super();
		// TODO Auto-generated constructor stub
	}

}
