package Model;

public interface Person {

	void setId(int id);

	int getId();

	void setName(String name);

	String getName();

	void setPhone(String phone);

	String getPhone();

	void setAddress(String address);

	String getAddress();
}
