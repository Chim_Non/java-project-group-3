package Model;

import java.util.Date;

public class Order {
	private int orderId;
	private int senderId;
	private int receiverId;
	private int staffId;
	private int orderType;
	private int orderZone;
	private float orderWeight;
	private String orderStatus;
	private Date orderDate;

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getSenderId() {
		return senderId;
	}

	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}

	public int getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}

	public int getStaffId() {
		return staffId;
	}

	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}

	public int getOrderType() {
		return orderType;
	}

	public void setOrderType(int orderType) {
		this.orderType = orderType;
	}

	public int getOrderZone() {
		return orderZone;
	}

	public void setOrderZone(int i) {
		this.orderZone = i;
	}

	public float getOrderWeight() {
		return orderWeight;
	}

	public void setOrderWeight(float orderWeight) {
		this.orderWeight = orderWeight;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Order(int orderId, int senderId, int receiverId, int staffId, int orderType, int orderZone,
			float orderWeight, String orderStatus, Date orderDate) {
		super();
		this.orderId = orderId;
		this.senderId = senderId;
		this.receiverId = receiverId;
		this.staffId = staffId;
		this.orderType = orderType;
		this.orderZone = orderZone;
		this.orderWeight = orderWeight;
		this.orderStatus = orderStatus;
		this.orderDate = orderDate;
	}

	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}

}
