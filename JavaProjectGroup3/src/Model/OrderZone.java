package Model;

public class OrderZone {
	int orderZoneId;
	String orderZoneName;
	float orderZoneRation;

	public int getOrderZoneId() {
		return orderZoneId;
	}

	public void setOrderZoneId(int orderZoneId) {
		this.orderZoneId = orderZoneId;
	}

	public String getOrderZoneName() {
		return orderZoneName;
	}

	public void setOrderZoneName(String orderZoneName) {
		this.orderZoneName = orderZoneName;
	}

	public float getOrderZoneRation() {
		return orderZoneRation;
	}

	public void setOrderZoneRation(float orderZoneRation) {
		this.orderZoneRation = orderZoneRation;
	}

	public OrderZone(int orderZoneId, String orderZoneName, float orderZoneRation) {
		super();
		this.orderZoneId = orderZoneId;
		this.orderZoneName = orderZoneName;
		this.orderZoneRation = orderZoneRation;
	}

	public OrderZone() {
		super();
		// TODO Auto-generated constructor stub
	}

}
