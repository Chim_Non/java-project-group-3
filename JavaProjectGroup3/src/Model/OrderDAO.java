package Model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Control.DBUtils;

public class OrderDAO {
	private Connection connection;

	public OrderDAO() {
		connection = DBUtils.getConnection();
	}

	public List<Order> getAllOrder() {
		List<Order> orders = new ArrayList<Order>();
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("call viewAllOrder()");
			while (rs.next()) {
				Order order = new Order();
				order.setOrderId(rs.getInt(1));
				order.setOrderType(rs.getInt(2));
				order.setOrderZone(rs.getInt(3));
				order.setSenderId(rs.getInt(4));
				order.setStaffId(rs.getInt(5));
				order.setOrderWeight(rs.getFloat(6));
				order.setOrderDate(rs.getDate("date"));
				order.setOrderStatus(rs.getString(8));
				order.setReceiverId(rs.getInt(9));
				orders.add(order);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return orders;
	}

	public void creatOrder(Order order) {
		try {
			PreparedStatement statement = connection.prepareStatement("call creatNewOrder(?,?,?,?,?,?)");
			statement.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void deleteOrderByID(int id) {
		try {
			PreparedStatement statement = connection.prepareStatement("call deleteOrderByID(?)");
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void updateOrder(Order order) throws IOException {
		try {
			PreparedStatement statement = connection.prepareStatement("call updateOrder(?,?,?,?,?,?)");
			statement.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public Order getOrderById(int orderID) {
		Order order = new Order();
		try {
			PreparedStatement statement = connection.prepareStatement("call getOrderById(?)");
			statement.setInt(1, orderID);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				order.setOrderId(rs.getInt(1));
				order.setOrderType(rs.getInt(2));
				order.setOrderZone(rs.getInt(3));
				order.setSenderId(rs.getInt(4));
				order.setStaffId(rs.getInt(5));
				order.setOrderWeight(rs.getFloat(6));
				order.setOrderDate(rs.getDate("date"));
				order.setOrderStatus(rs.getString(8));
				order.setReceiverId(rs.getInt(9));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return order;
	}

	public List<OrderType> getAllOrderType() {
		List<OrderType> orderTypes = new ArrayList<OrderType>();

		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("call viewAllOrderType()");
			while (rs.next()) {
				OrderType orderType = new OrderType();
				orderType.setOrderTypeId(rs.getInt(1));
				orderType.setOrderTypeName(rs.getString(2));
				orderType.setOrderTypeRatio(rs.getFloat(3));
				orderTypes.add(orderType);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return orderTypes;
	}

	public String getOrderTypeById(int typeID) {

		String orderType = null;
		try {
			PreparedStatement statement = connection.prepareStatement("call getOrderType(?)");
			statement.setInt(1, typeID);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				orderType = rs.getString(2);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return orderType;
	}

	public List<OrderZone> getAllOrderZone() {
		List<OrderZone> orderZones = new ArrayList<OrderZone>();

		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("call viewAllOrderType()");
			while (rs.next()) {
				OrderZone orderZone = new OrderZone();
				orderZone.setOrderZoneId(rs.getInt(1));
				orderZone.setOrderZoneName(rs.getString(2));
				orderZone.setOrderZoneRation(rs.getFloat(3));
				orderZones.add(orderZone);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return orderZones;
	}

	public String getOrderZoneById(int zoneID) {
		String orderZone = null;
		try {
			PreparedStatement statement = connection.prepareStatement("call getOrderZone(?)");
			statement.setInt(1, zoneID);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				orderZone = rs.getString(2);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return orderZone;
	}

	public int getNextOrderID() {
		int id = 0;
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select max(id_order) from order_table");
			while (rs.next()) {
				id = rs.getInt("max(id_order)");
			}
			id++;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

}
