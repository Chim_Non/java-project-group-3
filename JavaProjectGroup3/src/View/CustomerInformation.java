package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Frame;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import Control.ViewController;
import Model.Customer;
import Model.CustomerDAO;

import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class CustomerInformation {

	private JFrame frame;
	private JTextField cusId;
	private JTextField cusName;
	private JTextField cusAddress;
	private JTextField cusPhone;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustomerInformation window = new CustomerInformation();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CustomerInformation() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblCustomerInformation = new JLabel("Customer Information");
		lblCustomerInformation.setHorizontalAlignment(SwingConstants.CENTER);
		lblCustomerInformation.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblCustomerInformation.setBounds(10, 11, 414, 21);
		frame.getContentPane().add(lblCustomerInformation);

		
		CustomerManagement customerManagement = new CustomerManagement();
		cusId = new JTextField();
		cusId.setColumns(10);
		cusId.setBounds(191, 68, 114, 20);
		frame.getContentPane().add(cusId);
		cusId.disable();
		
		JLabel label_1 = new JLabel("Id:");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_1.setBounds(100, 71, 46, 14);
		frame.getContentPane().add(label_1);

		JLabel label_2 = new JLabel("Name:");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_2.setBounds(100, 99, 46, 14);
		frame.getContentPane().add(label_2);

		cusName = new JTextField();
		cusName.setColumns(10);
		cusName.setBounds(191, 96, 114, 20);
		frame.getContentPane().add(cusName);

		cusAddress = new JTextField();
		cusAddress.setColumns(10);
		cusAddress.setBounds(191, 158, 114, 20);
		frame.getContentPane().add(cusAddress);

		JLabel label_3 = new JLabel("Address:");
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_3.setBounds(100, 160, 57, 14);
		frame.getContentPane().add(label_3);

		JLabel label_4 = new JLabel("Phone:");
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_4.setBounds(100, 130, 46, 14);
		frame.getContentPane().add(label_4);

		cusPhone = new JTextField();
		cusPhone.setColumns(10);
		cusPhone.setBounds(191, 127, 114, 20);
		frame.getContentPane().add(cusPhone);

		JButton button = new JButton("Save");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CustomerManagement customerManagement = new CustomerManagement();
				CustomerDAO customerDAO = new CustomerDAO();
				Customer customer = new Customer();
				customer.setName(cusName.getText());
				customer.setPhone(cusPhone.getText());
				customer.setAddress(cusAddress.getText());
				if(Integer.parseInt(cusId.getText()) == (customerDAO.getMaxId())){
					customerDAO.creatCustomer(customer);
				}else{
					
					
					try {
						customerDAO.updateCustomer(Integer.parseInt(cusId.getText()),cusName.getText(),cusPhone.getText(),cusAddress.getText());
					} catch (NumberFormatException | IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				customerDAO.getAllCustomer();
				ViewController.moveFrame(frame, customerManagement.getFrame());				
				customerManagement.reloadTable();
				customerManagement.limitRows();
				
			}
		});
		button.setBounds(239, 214, 89, 23);
		frame.getContentPane().add(button);

		JButton button_1 = new JButton("Cancel");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CustomerManagement customerManagement = new CustomerManagement();
				ViewController.moveFrame(frame, customerManagement.getFrame());
			}
		});
		button_1.setBounds(66, 214, 89, 23);
		frame.getContentPane().add(button_1);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JTextField getCusId() {
		return cusId;
	}

	public void setCusId(JTextField cusId) {
		this.cusId = cusId;
	}

	public JTextField getCusName() {
		return cusName;
	}

	public void setCusName(JTextField cusName) {
		this.cusName = cusName;
	}

	public JTextField getCusAddress() {
		return cusAddress;
	}

	public void setCusAddress(JTextField cusAddress) {
		this.cusAddress = cusAddress;
	}

	public JTextField getCusPhone() {
		return cusPhone;
	}

	public void setCusPhone(JTextField cusPhone) {
		this.cusPhone = cusPhone;
	}

	
}
