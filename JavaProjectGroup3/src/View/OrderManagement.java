package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;

import Control.OrderController;
import Control.ViewController;
import Model.OrderDAO;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.FlowLayout;

public class OrderManagement extends JFrame {
	int selectedOrder = 0;

	private JFrame frame;
	private JTextField textField;
	private JTable orderTable;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OrderManagement window = new OrderManagement();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public OrderManagement() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 784, 471);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(109, 0, 649, 37);
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblOrderManagement = new JLabel("Order Management");
		lblOrderManagement.setBounds(10, 4, 152, 22);
		lblOrderManagement.setHorizontalAlignment(SwingConstants.CENTER);
		lblOrderManagement.setFont(new Font("Times New Roman", Font.BOLD, 18));
		panel.add(lblOrderManagement);

		textField = new JTextField();
		textField.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		textField.setBounds(172, 7, 274, 20);
		panel.add(textField);
		textField.setColumns(10);

		JButton btnNewButton = new JButton("Search");
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnNewButton.setBounds(559, 6, 80, 23);
		panel.add(btnNewButton);

		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(
				new String[] { "-- Select --", "Order ID", "Oder zone", "Order Status", "Order type" }));
		comboBox_1.setFont(new Font("Times New Roman", Font.PLAIN, 11));
		comboBox_1.setBounds(456, 7, 93, 20);
		panel.add(comboBox_1);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.setBounds(10, 48, 748, 339);
		frame.getContentPane().add(panel_1);

		OrderController orderController = new OrderController();
		panel_1.setLayout(new BorderLayout(0, 0));
		orderTable = new JTable();
		orderTable.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		orderTable.setBounds(1, 26, 450, 48);
		orderTable.setModel(new DefaultTableModel(orderController.getAllOrderData(), orderTableColumnName()));
		panel_1.add(orderTable, BorderLayout.CENTER);

		JScrollPane scrollPane = new JScrollPane(orderTable);
		scrollPane.setBounds(16, 40, 730, 203);
		panel_1.add(scrollPane);

		JButton btnBack = new JButton("Back");
		btnBack.setHorizontalAlignment(SwingConstants.RIGHT);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnBack.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnBack.setBounds(59, 398, 68, 23);
		frame.getContentPane().add(btnBack);

		JButton btnCreatNew = new JButton("Creat New");
		btnCreatNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderDAO dao = new OrderDAO();
				OrderInformation orderInformation = new OrderInformation();
				int id = dao.getNextOrderID();
				orderInformation.getOrderID().setText(Integer.toString(id));
				ViewController.moveFrame(frame, OrderInformation.getFrame());
			}
		});
		btnCreatNew.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnCreatNew.setBounds(186, 398, 105, 23);
		frame.getContentPane().add(btnCreatNew);

		JButton btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedRowIndex = orderTable.getSelectedRow();
				int idselect = (int) orderTable.getValueAt(selectedRowIndex, 0);
				OrderInformation orderInformation = new OrderInformation();
				orderInformation.getOrderID().setText(Integer.toString(idselect));
				ViewController.moveFrame(frame, orderInformation.getFrame());
			}
		});
		btnEdit.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnEdit.setBounds(350, 398, 89, 23);
		frame.getContentPane().add(btnEdit);

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedRowIndex = orderTable.getSelectedRow();
				int idselect = (int) orderTable.getValueAt(selectedRowIndex, 0);
				OrderDAO dao = new OrderDAO();
				OrderController orderController = new OrderController();
				dao.deleteOrderByID(idselect);
				// orderTable.setModel(new
				// DefaultTableModel(orderController.getAllOrderData(),
				// orderTableColumnName()));
				((DefaultTableModel) orderTable.getModel()).removeRow(orderTable.getSelectedRow());
			}
		});
		btnDelete.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnDelete.setBounds(498, 398, 89, 23);
		frame.getContentPane().add(btnDelete);

		JButton btnNext = new JButton("Next");
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNext.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnNext.setBounds(646, 398, 61, 23);
		frame.getContentPane().add(btnNext);

		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "---- Menu ----", "Staff", "Order", "Customer" }));
		comboBox.setBounds(10, 0, 89, 37);
		frame.getContentPane().add(comboBox);
	}

	private Object[] orderTableColumnName() {
		return new Object[] { "ID", "Order Type", "Zone", "Weight", "Staff", "Sender", "Receiver", "Date", "Status" };
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

}
