package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;  
import javax.swing.JTextField;

import Control.ViewController;
import Model.Staff;
import Model.StaffDAO;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class StaffInformation {

	private JFrame frame; 
	private JTextField txtstaffName;
	private JTextField txtstaffPhone;
	private JTextField txtstaffAddress;
	private JTextField txtstaffPass;
	private JButton btnCancel;
	private JButton btnSave;
	private JComboBox cbbstaffRole;
	private JTextField txtIDStaff;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StaffInformation window = new StaffInformation();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public StaffInformation() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 325);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblId = new JLabel("Pass:");
		lblId.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblId.setBounds(94, 185, 46, 14);
		frame.getContentPane().add(lblId);

		txtstaffName = new JTextField();
		txtstaffName.setBounds(185, 84, 114, 20);
		frame.getContentPane().add(txtstaffName);
		txtstaffName.setColumns(10);

		JLabel lblAccountInformation = new JLabel("Staff Information");
		lblAccountInformation.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblAccountInformation.setBounds(127, 11, 224, 21);
		frame.getContentPane().add(lblAccountInformation);

		txtstaffPhone = new JTextField();
		txtstaffPhone.setColumns(10);
		txtstaffPhone.setBounds(185, 115, 114, 20);
		frame.getContentPane().add(txtstaffPhone);

		JLabel lblName = new JLabel("Name:");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblName.setBounds(94, 86, 46, 14);
		frame.getContentPane().add(lblName);

		txtstaffAddress = new JTextField();
		txtstaffAddress.setColumns(10);
		txtstaffAddress.setBounds(185, 146, 114, 20);
		frame.getContentPane().add(txtstaffAddress);

		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblAddress.setBounds(94, 148, 57, 14);
		frame.getContentPane().add(lblAddress);

		txtstaffPass = new JTextField();
		txtstaffPass.setColumns(10);
		txtstaffPass.setBounds(185, 183, 114, 20);
		frame.getContentPane().add(txtstaffPass);

		JLabel lblPhone = new JLabel("Phone:");
		lblPhone.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPhone.setBounds(94, 117, 46, 14);
		frame.getContentPane().add(lblPhone);

		JLabel lblRole = new JLabel("Role:");
		lblRole.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblRole.setBounds(94, 216, 46, 14);
		frame.getContentPane().add(lblRole);

		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StaffManagement staffManagement = new StaffManagement();
				ViewController.moveFrame(frame, staffManagement.getFrame());

			}
		});
		btnCancel.setBounds(78, 252, 89, 23);
		frame.getContentPane().add(btnCancel);

		btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Staff staff = new Staff();
				StaffDAO dao = new StaffDAO();
				if ("".equals(txtstaffName.getText())) {
					JOptionPane.showMessageDialog(null, "Please enter name.");
				} else if ("".equals(txtstaffPhone.getText())) {
					JOptionPane.showMessageDialog(null, "Please enter phone.");
				} else if ("".equals(txtstaffAddress.getText())) {
					JOptionPane.showMessageDialog(null, "Please enter address.");
				} else if ("".equals(txtstaffPass.getText())) {
					JOptionPane.showMessageDialog(null, "Please enter pass.");
				} else {
					dao.insertStaff(txtstaffName.getText(), txtstaffPhone.getText(), txtstaffAddress.getText(),
							txtstaffPass.getText(), Integer.parseInt(cbbstaffRole.getSelectedItem().toString()));
					StaffManagement staffManagement = new StaffManagement();
					ViewController.moveFrame(frame, staffManagement.getFrame());
				}
			}
		});
		btnSave.setBounds(262, 252, 89, 23);
		frame.getContentPane().add(btnSave);

		cbbstaffRole = new JComboBox();
		cbbstaffRole.setBounds(186, 214, 28, 20);
		cbbstaffRole.addItem(1);
		cbbstaffRole.addItem(2);
		cbbstaffRole.addItem(3);
		frame.getContentPane().add(cbbstaffRole);

		txtIDStaff = new JTextField();
		txtIDStaff.setEnabled(false);
		txtIDStaff.setColumns(10);
		txtIDStaff.setBounds(185, 53, 114, 20);
		StaffDAO dao = new StaffDAO();
		int id = dao.getMaxId();
		txtIDStaff.setText(Integer.toString(id));
		frame.getContentPane().add(txtIDStaff);

		JLabel lblId_1 = new JLabel("ID:");
		lblId_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblId_1.setBounds(94, 55, 46, 14);
		frame.getContentPane().add(lblId_1);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
