package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import Control.PersonController;
import Control.ViewController;
import Model.Staff;
import Model.StaffDAO;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.FlowLayout;

public class StaffManagement {

	private JFrame frame;
	private JTable table;
	private JTextField txtSearch;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StaffManagement window = new StaffManagement();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public StaffManagement() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 766, 516);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblCreateAccount = new JLabel("Account Management");
		lblCreateAccount.setBounds(24, 6, 186, 21);
		lblCreateAccount.setFont(new Font("Tahoma", Font.BOLD, 17));
		frame.getContentPane().add(lblCreateAccount);

		JButton btnCreateAccount = new JButton("Create account");
		btnCreateAccount.setBounds(135, 245, 148, 23);
		btnCreateAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StaffInformation staffInformation = new StaffInformation();
				ViewController.moveFrame(frame, staffInformation.getFrame());
			}
		});
		frame.getContentPane().add(btnCreateAccount);
		table = new javax.swing.JTable();
		table.setBounds(1, 26, 680, 208);
		PersonController personController = new PersonController();

		table.setModel(new DefaultTableModel(personController.getStaffData(),
				new Object[] { "ID", "Name", "Phone", "Pass", "Address", "Role" }));
		// table.setModel(new DefaultTableModel(
		// new Object[][] {
		// {null, null, null, null, null, null},
		// {null, null, null, null, null, null},
		// },
		// new String[] {
		// "ID", "Name", "Phone","Pass", "Address", "Role"
		// }
		// ));
		JButton btnUpdateAccount = new JButton("Update account");
		btnUpdateAccount.setBounds(307, 245, 147, 23);
		btnUpdateAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// StaffInformation staffInformation = new StaffInformation();
				// ViewController.moveFrame(frame, staffInformation.getFrame());
				int i = table.getSelectedRow();
				String id = (String) table.getValueAt(i, 0);
				String name = (String) table.getValueAt(i, 1);
				String phone = (String) table.getValueAt(i, 2);
				String address = (String) table.getValueAt(i, 3);
				String pass = (String) table.getValueAt(i, 4);
				String role = (String) table.getValueAt(i, 5);
				int idd =Integer.parseInt(id);
				int role1 = Integer.parseInt(role);
				StaffInformation staffin = new StaffInformation();
				StaffDAO dao = new StaffDAO();
				dao.updateStaff(idd, name, phone, address, pass, role1);
			}
		});
		frame.getContentPane().add(btnUpdateAccount);
		JButton btnDeleteAccount = new JButton("Delete account");
		btnDeleteAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (table.getSelectedRow() >= 0) {
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					String id =  (String) table.getValueAt(table.getSelectedRow(), 0);
					model.removeRow(table.getSelectedRow());
					StaffDAO dao = new StaffDAO();
					int idd =Integer.parseInt(id);
					dao.deleteStaff(idd);

				}
//				else {
//				    JOptionPane.showMessageDialog(null, "You must select something");
//				}﻿
			}
		});
		btnDeleteAccount.setBounds(476, 245, 142, 23);
		frame.getContentPane().add(btnDeleteAccount);
		frame.getContentPane().add(table, BorderLayout.CENTER);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(24, 56, 699, 137);
		frame.getContentPane().add(scrollPane);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				StaffDAO dao = new StaffDAO();
				List<Staff> staffs = new ArrayList<Staff>();
				DefaultTableModel models = (DefaultTableModel) table.getModel();
				List<Integer> list = new ArrayList<Integer>();
				for (int j = 0; j < table.getRowCount(); j++) {
					list.add(Integer.parseInt(table.getValueAt(j, 0).toString()));
				}
				int min = Collections.min(list) - 11;
				if (min < 0) {
					models.setRowCount(0);
					staffs = dao.getDataNext(0);
					for (int i = 0; i < staffs.size(); i++) {
						String idd = Integer.toString(staffs.get(i).getId()).trim();
						String name = staffs.get(i).getName().trim();
						String phone = staffs.get(i).getPhone().trim();
						String address = staffs.get(i).getAddress().trim();
						String pass = staffs.get(i).getPass().trim();
						String role = Integer.toString(staffs.get(i).getRole()).trim();
						String st[] = { idd, name, phone, address, pass, role };
						models.addRow(st);
					}
				} else {
					staffs = dao.getDataNext(min);
					models.setRowCount(0);
					for (int i = 0; i < staffs.size(); i++) {
						String idd = Integer.toString(staffs.get(i).getId()).trim();
						String name = staffs.get(i).getName().trim();
						String phone = staffs.get(i).getPhone().trim();
						String address = staffs.get(i).getAddress().trim();
						String pass = staffs.get(i).getPass().trim();
						String role = Integer.toString(staffs.get(i).getRole()).trim();
						String st[] = { idd, name, phone, address, pass, role };
						models.addRow(st);
					}
				}
			}
		});
		btnBack.setBounds(24, 245, 86, 23);
		frame.getContentPane().add(btnBack);

		JButton btnNext = new JButton("Next");
		btnNext.setBounds(628, 245, 95, 23);
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				StaffDAO dao = new StaffDAO();
				List<Staff> staffs = new ArrayList<Staff>();
				DefaultTableModel models = (DefaultTableModel) table.getModel();
				int id = table.getRowCount();
				List<Integer> list = new ArrayList<Integer>();
				for (int j = 0; j < id; j++) {
					list.add(Integer.parseInt(table.getValueAt(j, 0).toString()));
				}
				int max = Collections.max(list);
				int min = Collections.min(list);
				if (id<10) {
					staffs = dao.getDataNext(min);
					for (int i = 0; i < staffs.size(); i++) {
						String idd = Integer.toString(staffs.get(i).getId()).trim();
						String name = staffs.get(i).getName().trim();
						String phone = staffs.get(i).getPhone().trim();
						String address = staffs.get(i).getAddress().trim();
						String pass = staffs.get(i).getPass().trim();
						String role = Integer.toString(staffs.get(i).getRole()).trim();
						String st[] = { idd, name, phone, address, pass, role };
						models.addRow(st);
					}
				}else{
					staffs = dao.getDataNext(max);
					models.setRowCount(0);
					for (int i = 0; i < staffs.size(); i++) {
						String idd = Integer.toString(staffs.get(i).getId()).trim();
						String name = staffs.get(i).getName().trim();
						String phone = staffs.get(i).getPhone().trim();
						String address = staffs.get(i).getAddress().trim();
						String pass = staffs.get(i).getPass().trim();
						String role = Integer.toString(staffs.get(i).getRole()).trim();
						String st[] = { idd, name, phone, address, pass, role };
						models.addRow(st);
					}
				}
			}
		});
		frame.getContentPane().add(btnNext);

		txtSearch = new JTextField();
		txtSearch.setBounds(220, 9, 218, 20);
		frame.getContentPane().add(txtSearch);
		txtSearch.setColumns(10);

		final JComboBox cbbSearch = new JComboBox();
		cbbSearch.setBounds(466, 9, 66, 20);
		cbbSearch.addItem("id");
		cbbSearch.addItem("name");
		cbbSearch.addItem("phone");
		frame.getContentPane().add(cbbSearch);

		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StaffDAO dao = new StaffDAO();
				List<Staff> staffs = new ArrayList<Staff>();
				DefaultTableModel models = (DefaultTableModel) table.getModel();
				if ("id".equals(cbbSearch.getSelectedItem().toString())) {
					int id = Integer.parseInt(txtSearch.getText());
					staffs = dao.getStaffById(id);
					models.setRowCount(0);
					for (int i = 0; i < staffs.size(); i++) {
					String idd = Integer.toString(staffs.get(i).getId()).trim();
					String name = staffs.get(i).getName().trim();
					String phone = staffs.get(i).getPhone().trim();
					String address = staffs.get(i).getAddress().trim();
					String pass = staffs.get(i).getPass().trim();
					String role = Integer.toString(staffs.get(i).getRole()).trim();
					String st[] = { idd, name, phone, address, pass, role };
					models.addRow(st);
					}
				} else if ("name".equals(cbbSearch.getSelectedItem().toString())) {
					String nameSearch = txtSearch.getText();
					staffs = dao.getStaffByName(nameSearch);
					models.setRowCount(0);
					for (int i = 0; i < staffs.size(); i++) {
						String idd = Integer.toString(staffs.get(i).getId()).trim();
						String name = staffs.get(i).getName().trim();
						String phone = staffs.get(i).getPhone().trim();
						String address = staffs.get(i).getAddress().trim();
						String pass = staffs.get(i).getPass().trim();
						String role = Integer.toString(staffs.get(i).getRole()).trim();
						String st[] = { idd, name, phone, address, pass, role };
						models.addRow(st);
						}
				} else if ("phone".equals(cbbSearch.getSelectedItem().toString())) {
					String phoneSearch = txtSearch.getText();
					staffs = dao.getStaffByPhone(phoneSearch);
					models.setRowCount(0);
					for (int i = 0; i < staffs.size(); i++) {
						String idd = Integer.toString(staffs.get(i).getId()).trim();
						String name = staffs.get(i).getName().trim();
						String phone = staffs.get(i).getPhone().trim();
						String address = staffs.get(i).getAddress().trim();
						String pass = staffs.get(i).getPass().trim();
						String role = Integer.toString(staffs.get(i).getRole()).trim();
						String st[] = { idd, name, phone, address, pass, role };
						models.addRow(st);
						}
				}
			}
		});
		btnSearch.setBounds(542, 8, 95, 23);
		frame.getContentPane().add(btnSearch);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

}
