package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import Control.ViewController;
import Model.Order;
import Model.OrderDAO;
import Model.OrderType;
import Model.OrderZone;

import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.DefaultComboBoxModel;

public class OrderInformation {

	private static JFrame frame;
	private JTextField orderID;
	private JTextField orderStaff;
	private JTextField orderWeight;
	private JTextField orderDate;
	private JTextField senderID;
	private JTextField senderName;
	private JTextField senderPhone;
	private JTextField senderAddress;
	private JTextField receiverID;
	private JTextField receiverName;
	private JTextField receiverPhone;
	private JTextField receiverAddress;
	private JTextField staffID;

	public JTextField getOrderID() {
		return orderID;
	}

	public void setOrderID(JTextField orderID) {
		this.orderID = orderID;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OrderInformation window = new OrderInformation();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public OrderInformation() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 590, 470);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(10, 0, 437, 37);
		frame.getContentPane().add(panel);

		JLabel lblOrderInformation = new JLabel("Order Information");
		lblOrderInformation.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblOrderInformation);
		lblOrderInformation.setFont(new Font("Times New Roman", Font.BOLD, 18));

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(Color.LIGHT_GRAY));
		panel_1.setBounds(20, 48, 532, 119);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);

		JLabel lblId = new JLabel("ID :");
		lblId.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		lblId.setBounds(10, 11, 33, 16);
		panel_1.add(lblId);

		JLabel lblOrderType = new JLabel("Type :");
		lblOrderType.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		lblOrderType.setBounds(10, 49, 75, 16);
		panel_1.add(lblOrderType);

		JLabel lblStaff = new JLabel("Staff :");
		lblStaff.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		lblStaff.setBounds(10, 87, 46, 14);
		panel_1.add(lblStaff);

		orderID = new JTextField();
		orderID.setEnabled(false);
		orderID.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		orderID.setBounds(80, 9, 166, 20);
		panel_1.add(orderID);
		orderID.setColumns(10);

		final JComboBox orderType = new JComboBox();

		//orderType.setModel(new DefaultComboBoxModel<>(getOrderType()));

		DefaultComboBoxModel<Object[][]> ordertype = new DefaultComboBoxModel<>();
		ordertype = (DefaultComboBoxModel<Object[][]>) getOrderType();

		//List<String[][]> typeList = getOrderType();

		

		orderType.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		orderType.setBounds(80, 47, 166, 20);
		panel_1.add(orderType);

		orderStaff = new JTextField();
		orderStaff.setEnabled(false);
		orderStaff.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		orderStaff.setBounds(107, 84, 103, 20);
		panel_1.add(orderStaff);
		orderStaff.setColumns(10);

		JLabel lblWeight = new JLabel("Weight :");
		lblWeight.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		lblWeight.setBounds(281, 12, 59, 14);
		panel_1.add(lblWeight);

		JLabel lblZone = new JLabel("Zone :");
		lblZone.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		lblZone.setBounds(285, 50, 46, 14);
		panel_1.add(lblZone);

		JLabel lblDate = new JLabel("Date :");
		lblDate.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		lblDate.setBounds(285, 90, 46, 14);
		panel_1.add(lblDate);

		orderWeight = new JTextField();
		orderWeight.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		orderWeight.setBounds(350, 7, 172, 20);
		panel_1.add(orderWeight);
		orderWeight.setColumns(10);

		final JComboBox orderZone = new JComboBox();
		orderZone.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		orderZone.setBounds(350, 49, 172, 20);
		panel_1.add(orderZone);

		orderDate = new JTextField();
		orderDate.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		orderDate.setBounds(350, 87, 172, 20);
		panel_1.add(orderDate);
		orderDate.setColumns(10);

		staffID = new JTextField();
		staffID.setBounds(80, 84, 26, 20);
		panel_1.add(staffID);
		staffID.setColumns(10);

		JButton btnNewButton = new JButton("");
		btnNewButton.setBounds(213, 83, 33, 21);
		panel_1.add(btnNewButton);

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(Color.LIGHT_GRAY));
		panel_2.setBounds(20, 178, 255, 198);
		frame.getContentPane().add(panel_2);
		panel_2.setLayout(null);

		JLabel lblNewLabel = new JLabel("Sender Information");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblNewLabel.setBounds(10, 6, 237, 14);
		panel_2.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Sender ID:");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		lblNewLabel_1.setBounds(10, 49, 58, 14);
		panel_2.add(lblNewLabel_1);

		JLabel lblName = new JLabel("Name :");
		lblName.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		lblName.setBounds(10, 86, 39, 14);
		panel_2.add(lblName);

		JLabel lblPhone = new JLabel("Phone :");
		lblPhone.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		lblPhone.setBounds(10, 124, 46, 14);
		panel_2.add(lblPhone);

		JLabel lblAddress = new JLabel("Address :");
		lblAddress.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		lblAddress.setBounds(10, 165, 58, 14);
		panel_2.add(lblAddress);

		senderID = new JTextField();
		senderID.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		senderID.setBounds(78, 46, 95, 23);
		panel_2.add(senderID);
		senderID.setColumns(10);

		senderName = new JTextField();
		senderName.setEnabled(false);
		senderName.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		senderName.setBounds(77, 83, 170, 20);
		panel_2.add(senderName);
		senderName.setColumns(10);

		senderPhone = new JTextField();
		senderPhone.setEnabled(false);
		senderPhone.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		senderPhone.setBounds(78, 121, 169, 20);
		panel_2.add(senderPhone);
		senderPhone.setColumns(10);

		senderAddress = new JTextField();
		senderAddress.setEnabled(false);
		senderAddress.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		senderAddress.setBounds(78, 162, 169, 20);
		panel_2.add(senderAddress);
		senderAddress.setColumns(10);

		JButton btnLoadSender = new JButton("Load");
		btnLoadSender.setFont(new Font("Times New Roman", Font.PLAIN, 11));
		btnLoadSender.setBounds(183, 45, 62, 23);
		panel_2.add(btnLoadSender);

		JPanel panel_3 = new JPanel();
		panel_3.setLayout(null);
		panel_3.setBorder(new LineBorder(Color.LIGHT_GRAY));
		panel_3.setBounds(296, 178, 257, 198);
		frame.getContentPane().add(panel_3);

		JLabel lblReceiverInformation = new JLabel("Receiver Information");
		lblReceiverInformation.setHorizontalAlignment(SwingConstants.CENTER);
		lblReceiverInformation.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblReceiverInformation.setBounds(10, 6, 237, 14);
		panel_3.add(lblReceiverInformation);

		JLabel lblReceiverId = new JLabel("Receiver ID:");
		lblReceiverId.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		lblReceiverId.setBounds(10, 49, 65, 14);
		panel_3.add(lblReceiverId);

		JLabel label_2 = new JLabel("Name :");
		label_2.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		label_2.setBounds(10, 86, 39, 14);
		panel_3.add(label_2);

		JLabel label_3 = new JLabel("Phone :");
		label_3.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		label_3.setBounds(10, 124, 46, 14);
		panel_3.add(label_3);

		JLabel label_4 = new JLabel("Address :");
		label_4.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		label_4.setBounds(10, 165, 58, 14);
		panel_3.add(label_4);

		receiverID = new JTextField();
		receiverID.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		receiverID.setColumns(10);
		receiverID.setBounds(78, 46, 96, 23);
		panel_3.add(receiverID);

		receiverName = new JTextField();
		receiverName.setEnabled(false);
		receiverName.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		receiverName.setColumns(10);
		receiverName.setBounds(77, 83, 170, 20);
		panel_3.add(receiverName);

		receiverPhone = new JTextField();
		receiverPhone.setEnabled(false);
		receiverPhone.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		receiverPhone.setColumns(10);
		receiverPhone.setBounds(78, 121, 169, 20);
		panel_3.add(receiverPhone);

		receiverAddress = new JTextField();
		receiverAddress.setEnabled(false);
		receiverAddress.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		receiverAddress.setColumns(10);
		receiverAddress.setBounds(78, 162, 169, 20);
		panel_3.add(receiverAddress);

		JButton btnLoadReceiver = new JButton("Load");
		btnLoadReceiver.setFont(new Font("Times New Roman", Font.PLAIN, 11));
		btnLoadReceiver.setBounds(184, 45, 63, 23);
		panel_3.add(btnLoadReceiver);

		final JComboBox orderStatus = new JComboBox();
		orderStatus.setFont(new Font("Times New Roman", Font.BOLD, 13));
		orderStatus.setModel(new DefaultComboBoxModel<>(getOrderStatus()));
		orderStatus.setSelectedItem("Done");
		orderStatus.setBounds(457, 0, 107, 37);
		frame.getContentPane().add(orderStatus);

		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				OrderManagement orderManagement = new OrderManagement();
				Order order = new Order();

				order.setOrderId(Integer.parseInt(orderID.getText()));
				order.setOrderType(Integer.parseInt(orderType.getSelectedItem().toString()));
				order.setStaffId(Integer.parseInt(orderStaff.getText()));
				order.setOrderWeight(Float.parseFloat(orderWeight.getText()));
				order.setOrderZone(Integer.parseInt(orderZone.getSelectedItem().toString()));
				DateFormat dateFomat = new SimpleDateFormat("yyyy/MM/dd");

				try {
					order.setOrderDate(dateFomat.parse(orderDate.getText()));
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				order.setOrderStatus(orderStatus.getSelectedItem().toString());
				order.setSenderId(Integer.parseInt(senderID.getText()));
				order.setReceiverId(Integer.parseInt(receiverID.getText()));

				ViewController.moveFrame(frame, orderManagement.getFrame());
			}
		});
		btnSave.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnSave.setBounds(317, 397, 89, 23);
		frame.getContentPane().add(btnSave);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderManagement orderManagement = new OrderManagement();
				ViewController.moveFrame(frame, orderManagement.getFrame());
			}
		});
		btnCancel.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnCancel.setBounds(169, 397, 89, 23);
		frame.getContentPane().add(btnCancel);

		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_4.setBounds(10, 41, 554, 345);
		frame.getContentPane().add(panel_4);

	}

	public static JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public Object[] getOrderStatus() {
		return new Object[] { "Prepare", "Sending", "Done" };
	}

	public List<String[][]> getOrderType() {

		List<String[][]> typeList = new ArrayList<String[][]>();
		OrderDAO orderDAO = new OrderDAO();
		List<OrderType> orderTypes = new ArrayList<OrderType>();
		orderTypes = orderDAO.getAllOrderType();

		String[][] rowData = new String[orderTypes.size()][2];
		for (int i = 0; i < orderTypes.size(); i++) {
			rowData[i][0] = String.valueOf(orderTypes.get(i).getOrderTypeId());
			rowData[i][1] = orderTypes.get(i).getOrderTypeName();
			typeList.add(rowData);
		}

		return typeList;
	}

	public Object[][] getOrderZone() {
		OrderDAO orderDAO = new OrderDAO();
		List<OrderZone> orderZones = new ArrayList<OrderZone>();
		orderZones = orderDAO.getAllOrderZone();

		Object[][] rowData = new Object[orderZones.size()][2];
		for (int i = 0; i < orderZones.size(); i++) {
			rowData[i][0] = orderZones.get(i).getOrderZoneId();
			rowData[i][1] = orderZones.get(i).getOrderZoneName();
		}

		return rowData;
	}
}
