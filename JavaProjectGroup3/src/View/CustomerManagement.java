package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import Control.PersonController;
import Control.ViewController;
import Model.Customer;
import Model.CustomerDAO;

import java.awt.event.ActionListener;
import java.io.Console;
import java.util.ArrayList;
import java.util.Collections;
import java.awt.event.ActionEvent;
import java.awt.FlowLayout;
import javax.swing.DefaultComboBoxModel;

public class CustomerManagement {

	private JFrame frame;
	private JTextField textSearch;
	private JTable customerManager;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustomerManagement window = new CustomerManagement();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CustomerManagement() {
		initialize();

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 785, 495);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		final int rows = 11;

		JPanel gui = new JPanel(new BorderLayout(3, 3));

		// final JScrollPane scrollPane = new JScrollPane(customerManager,
		// JScrollPane.VERTICAL_SCROLLBAR_NEVER,
		// JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		PersonController personController = new PersonController();
		final JScrollPane scrollPane = new JScrollPane(customerManager);
		scrollPane.setBounds(16, 40, 730, 275);
		frame.getContentPane().add(scrollPane);
		customerManager = new JTable();
		scrollPane.setViewportView(customerManager);
		customerManager.setModel(new DefaultTableModel(personController.getCustomerData(),
				new Object[] { "ID", "Name", "Phone", "Address" }));

		limitRows();

		JLabel lblCustomerManagement = new JLabel("Customer Management");
		lblCustomerManagement.setBounds(116, 4, 198, 21);
		lblCustomerManagement.setFont(new Font("Tahoma", Font.BOLD, 17));
		frame.getContentPane().add(lblCustomerManagement);

		textSearch = new JTextField();
		textSearch.setBounds(421, 7, 228, 20);
		textSearch.setColumns(10);
		frame.getContentPane().add(textSearch);

		JButton button = new JButton("Search");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				filter(textSearch.getText());
			}
		});
		button.setBounds(659, 6, 87, 23);
		frame.getContentPane().add(button);

		JPanel navigation = new JPanel(new FlowLayout(FlowLayout.CENTER));

		JButton back = new JButton("Back");
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int height = customerManager.getRowHeight()*(rows-1);
                JScrollBar bar = scrollPane.getVerticalScrollBar();
                bar.setValue( bar.getValue()-height );
			}
		});
		back.setBounds(16, 325, 95, 23);
		frame.getContentPane().add(back);

		JButton button_2 = new JButton("Create account");
		button_2.setBounds(131, 326, 149, 23);
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CustomerInformation customerInformation = new CustomerInformation();
				PersonController personController = new PersonController();
				ViewController.moveFrame(frame, customerInformation.getFrame());
				CustomerDAO customerDAO = new CustomerDAO();
				customerInformation.getCusId().setText(Integer.toString(customerDAO.getMaxId()));
			}
		});
		frame.getContentPane().add(button_2);

		JButton button_3 = new JButton("Update account");
		button_3.setBounds(304, 325, 149, 23);
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CustomerInformation customerInformation = new CustomerInformation();
				ViewController.moveFrame(frame, customerInformation.getFrame());
				customerInformation.getCusId().setText(
						Integer.toString((int) customerManager.getValueAt(customerManager.getSelectedRow(), 0)));
				customerInformation.getCusName()
						.setText((String) customerManager.getValueAt(customerManager.getSelectedRow(), 1));
				customerInformation.getCusPhone()
						.setText((String) (customerManager.getValueAt(customerManager.getSelectedRow(), 2)));
				customerInformation.getCusAddress()
						.setText((String) (customerManager.getValueAt(customerManager.getSelectedRow(), 3)));
			}
		});
		frame.getContentPane().add(button_3);

		JButton button_4 = new JButton("Delete account");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CustomerDAO customerDAO = new CustomerDAO();
				customerDAO.deleteCustomer((int) customerManager.getValueAt(customerManager.getSelectedRow(), 0));
				((DefaultTableModel) customerManager.getModel()).removeRow(customerManager.getSelectedRow());
			}
		});
		button_4.setBounds(478, 325, 145, 23);
		frame.getContentPane().add(button_4);

		JButton next = new JButton("Next");
		next.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int height = customerManager.getRowHeight()*(rows-1);
                JScrollBar bar = scrollPane.getVerticalScrollBar();
                bar.setValue( bar.getValue()+height );
			}
		});
		next.setBounds(651, 325, 95, 23);
		frame.getContentPane().add(next);

		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] { "---Menu---", "Staff", "Order", "Customer", "Report" }));
		comboBox_1.setBounds(16, 7, 90, 20);
		frame.getContentPane().add(comboBox_1);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public void reloadTable() {
		PersonController personController = new PersonController();
		customerManager.setModel(new DefaultTableModel(personController.getCustomerData(),
				new Object[] { "ID", "Name", "Phone", "Address" }));
	}

	public void limitRows() {
		DefaultTableModel dtm = (DefaultTableModel) customerManager.getModel();
		dtm.setRowCount(5);
		customerManager.setModel(dtm);
	}

	private void filter(String query) {
		PersonController personController = new PersonController();
		TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(new DefaultTableModel(
				personController.getCustomerData(), new Object[] { "ID", "Name", "Phone", "Address" }));
		customerManager.setRowSorter(tr);
		// table.convertRowIndexToView(0);

		tr.setRowFilter(RowFilter.regexFilter(query));

	}
}
