package Control;

import Model.OrderDAO;

import java.util.ArrayList;
import java.util.List;
import Model.Order;
import Model.Staff;
import Model.StaffDAO;
import Model.Customer;
import Model.CustomerDAO;

public class OrderController {

	public Object[][] getAllOrderData() {
		OrderDAO orderDAO = new OrderDAO();
		List<Order> orders = new ArrayList<Order>();
		orders = orderDAO.getAllOrder();

		Staff staff = new Staff();
		StaffDAO staffDAO = new StaffDAO();

		Customer customer1 = new Customer();
		Customer customer2 = new Customer();
		CustomerDAO customerDAO = new CustomerDAO();

		Object[][] rowData = new Object[orders.size()][9];
		for (int i = 0; i < orders.size(); i++) {
			rowData[i][0] = orders.get(i).getOrderId();

			int orderType = orders.get(i).getOrderType();
			rowData[i][1] = orderDAO.getOrderTypeById(orderType);

			int orderZone = orders.get(i).getOrderZone();
			rowData[i][2] = orderDAO.getOrderZoneById(orderZone);

			rowData[i][3] = orders.get(i).getOrderWeight();

			staff = staffDAO.getStaffById(orders.get(i).getStaffId());
			rowData[i][4] = staff.getName();

			customer1 = customerDAO.getCustomerById(orders.get(i).getSenderId());
			rowData[i][5] = customer1.getName();

			customer2 = customerDAO.getCustomerById(orders.get(i).getReceiverId());
			rowData[i][6] = customer2.getName();

			rowData[i][7] = orders.get(i).getOrderDate();

			rowData[i][8] = orders.get(i).getOrderStatus();
		}
		return rowData;
	}
//
//	public Object[] getOrderDataByID(int id) {
//		Object[] orderData = new Object[9];
//		OrderDAO orderDAO = new OrderDAO();
//		Order order = new Order();
//		return orderData;
//	}
}
