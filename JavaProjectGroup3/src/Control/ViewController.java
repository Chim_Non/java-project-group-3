package Control;

import javax.swing.JFrame;

public class ViewController {
	public static void moveFrame(JFrame fromFrame, JFrame toFrame){
		fromFrame.dispose();
		toFrame.setVisible(true);
	}
}
