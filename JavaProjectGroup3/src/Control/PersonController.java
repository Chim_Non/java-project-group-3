package Control;

import java.util.ArrayList;
import java.util.List;

import Model.Staff;
import Model.StaffDAO;

import java.util.ArrayList;
import java.util.List;

import Model.Customer;
import Model.CustomerDAO;
import View.CustomerManagement;

public class PersonController {
	public Object[][] getCustomerData() {
		 CustomerDAO customerDAO = new CustomerDAO();
		 List<Customer> customers = new ArrayList<Customer>();
		 customers = customerDAO.getAllCustomer();
		 
		 Object[][] rowData = new Object[customers.size()][4];
		 for (int i = 0; i < customers.size(); i++) {
		 rowData[i][0] = customers.get(i).getId();
		 rowData[i][1] = customers.get(i).getName();
		 rowData[i][2] = customers.get(i).getPhone();
		 rowData[i][3] = customers.get(i).getAddress();
		 }
		 return rowData;
	}

	
		public Object[][] getStaffData() {
			 StaffDAO dao = new StaffDAO();
			 List<Staff> staffs = new ArrayList<Staff>();
			 staffs = dao.getAllStaff();
			 
			 Object[][] rowData = new Object[staffs.size()][6];
			 for (int i = 0; i < staffs.size(); i++) {
			 rowData[i][0] = staffs.get(i).getId();
			 rowData[i][1] = staffs.get(i).getName();
			 rowData[i][2] = staffs.get(i).getPhone();
			 rowData[i][3] = staffs.get(i).getAddress();
			 rowData[i][4] = staffs.get(i).getPass();
			 rowData[i][5] = staffs.get(i).getRole();
			 }
			 return rowData;
		}
//	public Object[][] getCustomerData() {
//		 CustomerDAO customerDAO = new CustomerDAO();
//		 List<Customer> customers = new ArrayList<Customer>();
//		 customers = customerDAO.getAllCustomer();
//		 
//		 Object[][] rowData = new Object[customers.size()][4];
//		 for (int i = 0; i < customers.size(); i++) {
//		 rowData[i][0] = customers.get(i).getId();
//		 rowData[i][1] = customers.get(i).getName();
//		 rowData[i][2] = customers.get(i).getPhone();
//		 rowData[i][3] = customers.get(i).getAddress();
//		 }
//		 return rowData;
//	}

}
